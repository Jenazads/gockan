package gockan

import (
  "context";
  "reflect";
  "net/url";
  "net/http";
  "encoding/json";
  "github.com/google/go-querystring/query";
)

// a instance for datastore and packages service
type gockanService struct {
  gockan *Gockan
}

// resources for CKAN
type ResourcesName map[string]string

// Headers to make a request to CKAN
type RequestHeaders map[string]string

// DataStoreMetadata holds the table metadata for a given CKAN datastore.
type GockanDataStoreMetadata struct {
  ID      string `json:"_id"`
  AliasOf string `json:"alias_of"`
  Name    string `json:"name"`
  OID     int    `json:"oid"`
}

// GockanPackages handles communication with the package related methods of
// the CKAN API.
type GockanPackages gockanService

// GockanDataStore handles communication with the datastore methods
// of the CKAN API.
type GockanDataStore gockanService

// Is used to save response from CKAN API in a RawMessage (text).
type jsonResponse struct {
  IsSuccess bool             `json:"success"`
  Result    json.RawMessage  `json:"result"`
  Error     *GockanError     `json:"error"`
  Help      string           `json:"help"`
}

// CKAN has 2 querys in returned data, limit and offset
// limit is for specifies a number of objects that will be returned.
// offset is to select a point of start to select results
type QueryOptions struct {
  Limit   int   `url:"limit,omitempty"`
  Offset  int   `url:"offset,omitempty"`
}

// Write in JSON Format
func (o *Gockan) ParseJsonResponse(response interface{}, w http.ResponseWriter) {
  json, err := json.Marshal(response)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }
  w.WriteHeader(http.StatusOK)
  w.Write(json)
}

// Adds limit, offset or other options to the given path and make the request
func addQueryOptions(queryURL string, opt interface{}) (string, error) {
  obj := reflect.ValueOf(opt)

  // verify is object is not nil
  if obj.Kind() == reflect.Ptr && obj.IsNil() {
    return queryURL, nil
  }

  // verify queryURL
  origURL, err := url.Parse(queryURL)
  if err != nil {
    return queryURL, err
  }

  origValues := origURL.Query()

  newValues, err := query.Values(opt)
  if err != nil {
    return queryURL, err
  }

  // get query values
  for key, value := range newValues {
    origValues[key] = value
  }

  origURL.RawQuery = origValues.Encode()
  return origURL.String(), nil
}

// TeableMetadata fetches the datastore table metadata for a given CKAN
// instance.
func (s *GockanDataStore) TableMetadata(ctx context.Context, opt *QueryOptions) ([]GockanDataStoreMetadata, *http.Response, error) {
  path := "action/datastore_search?resource_id=_table_metadata"
  path, err := addQueryOptions(path, opt)
  if err != nil {
    return nil, nil, err
  }

  req, err := s.gockan.GockanRequest("GET", path, nil)
  if err != nil {
    return nil, nil, err
  }

  var result struct {
  Records []GockanDataStoreMetadata `json:"records"`
  }
  resp, err := s.gockan.Do(ctx, req, &result)
  if err != nil {
    return result.Records, resp, err
  }

  return result.Records, resp, nil
}

// List all of the available packages names for a given CKAN instance.
func (s *GockanPackages) List(ctx context.Context, opt *QueryOptions) ([]string, *http.Response, error) {
  path := "action/package_list"
  path, err := addQueryOptions(path, opt)
  if err != nil {
    return nil, nil, err
  }

  req, err := s.gockan.GockanRequest("GET", path, nil)
  if err != nil {
    return nil, nil, err
  }

  var packages []string
  resp, err := s.gockan.Do(ctx, req, &packages)
  if err != nil {
    return packages, resp, err
  }

  return packages, resp, nil
}
