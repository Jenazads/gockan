package gockan

import (
  "errors";
)

// List of errors constants
var (
  //'200' => 'OK',
  GockanErr301           = errors.New("Moved Permanently.\n")
  GockanErr400           = errors.New("Bad request.\n")
  GockanErr401           = errors.New("Not Authorized.\n")
  GockanErr403           = errors.New("Forbidden.\n")
  GockanErr404           = errors.New("Not Found.\n")
  GockanErr409           = errors.New("Conflicts, could be tha name already exists.\n")
  GockanErr500           = errors.New("Service Error.\n")
  GockanErrNotSetBaseURL = errors.New("Cannot Set Base URL.\n")
)

// error returned by an API request
type GockanError struct {
  Type    string `json:"__type"`
  Message string `json:"message"`
}

// return error message returned by API using the Error interface{}
func (e *GockanError) Error() string {
  return e.Message
}

//   Catch http request error like 40X or 500 and replaces by 200 "ok" with a error message and success.
type CatchError struct{
  errorType  int
}

// catch and convert error message with success
func (c *CatchError) EvalError() error {
  if c.errorType == 301 {
    return GockanErr301
  } else if c.errorType == 400 {
    return GockanErr400
  } else if c.errorType == 401 {
    return GockanErr401
  } else if c.errorType == 403 {
    return GockanErr403
  } else if c.errorType == 404 {
    return GockanErr404
  } else if c.errorType == 409 {
    return GockanErr409
  } else if c.errorType == 500 {
    return GockanErr500
  } else {
    return errors.New("Request malformed and response body is empty.\n")
  }
}
