package gockan

import (
  "time";
  "net/url";
  "net/http";
)

// Gockan struct
type Gockan struct {
  apiKey         string             // apiKey     ==> /api/
  apiVersion     string             // apiVersion ==> 3
  resources      ResourcesName      // list of resources ckan
  reqHeaders     RequestHeaders     // request headers
  
  ckanClient     *http.Client       // http client to connect with CKAN
  baseURL        *url.URL           // http://demo.ckan.org/
  userAgent      string             // Ckan user agent for client Ckan_client/clientversion
  clientVersion  string             // clientVersion
  dataStore      *GockanDataStore   // Data Stores
  packages       *GockanPackages    // Packages
}

// Creates New Gockan Instance API client by default
func NewGockan(baseURL string) (*Gockan, error) {

  GockanObject := &Gockan{
                    ckanClient: http.DefaultClient,
                    userAgent: "gockan/"+"1",
                  }
//                  "https://demo.ckan.org/api/3/"
  GockanObject.SetApiVersion("3")
  GockanObject.SetApiKey("api/"+GockanObject.GetApiVersion())
  err := GockanObject.SetBaseURL(baseURL + GockanObject.GetApiKey())
  if err != nil{
    return nil, GockanErrNotSetBaseURL
  }
  GockanObject.SetDataStore(&GockanDataStore{GockanObject})
  GockanObject.SetPackages(&GockanPackages{GockanObject})
  GockanObject.SetRequestHeaders()
  
  return GockanObject, nil
}

// Creates New Gockan Instance API client with http parameters
func NewGockanHttpOptions(baseURL string, httpClient *http.Client) (*Gockan, error) {
  if httpClient == nil {
    httpClient = http.DefaultClient
  }

  GockanObject := &Gockan{
                    ckanClient: httpClient,
                    userAgent: "gockan/"+"1",
                  }
  GockanObject.SetApiVersion("3")
  GockanObject.SetApiKey("api/"+GockanObject.GetApiVersion())
  err := GockanObject.SetBaseURL(baseURL + GockanObject.GetApiKey())
  if err != nil{
    return nil, GockanErrNotSetBaseURL
  }
  GockanObject.SetDataStore(&GockanDataStore{GockanObject})
  GockanObject.SetPackages(&GockanPackages{GockanObject})
  GockanObject.SetRequestHeaders()

  return GockanObject, nil
}

// Creates New Gockan Instance API client with useragent parameters
func NewGockanAgentOptions(baseURL, userAgent, clientVersion string, httpClient *http.Client) (*Gockan, error) {
  if httpClient == nil {
    httpClient = http.DefaultClient
  }

  GockanObject := &Gockan{
                    ckanClient: httpClient,
                    userAgent: userAgent+clientVersion,
                  }
  GockanObject.SetApiVersion("3")
  GockanObject.SetApiKey("api/"+GockanObject.GetApiVersion())
  err := GockanObject.SetBaseURL(baseURL + GockanObject.GetApiKey())
  if err != nil{
    return nil, GockanErrNotSetBaseURL
  }
  GockanObject.SetDataStore(&GockanDataStore{GockanObject})
  GockanObject.SetPackages(&GockanPackages{GockanObject})
  GockanObject.SetRequestHeaders()

  return GockanObject, nil
}

// Creates New Gockan Instance API client with apikey parameters
func NewGockanApiOptions(baseURL, apiKey, apiVersion, userAgent, clientVersion string, httpClient *http.Client) (*Gockan, error) {
  if httpClient == nil {
    httpClient = http.DefaultClient
  }

  GockanObject := &Gockan{
                    ckanClient: httpClient,
                    userAgent: userAgent+clientVersion,
                  }
  GockanObject.SetApiVersion(apiVersion)
  GockanObject.SetApiKey(apiKey+GockanObject.GetApiVersion())
  err := GockanObject.SetBaseURL(baseURL + GockanObject.GetApiKey())
  if err != nil{
    return nil, GockanErrNotSetBaseURL
  }
  GockanObject.SetDataStore(&GockanDataStore{GockanObject})
  GockanObject.SetPackages(&GockanPackages{GockanObject})
  GockanObject.SetRequestHeaders()

  return GockanObject, nil
}

// set resources
func (o *Gockan) SetResourcesName(r ResourcesName)(){
  o.resources = r
}

// get resources
func (o *Gockan) GetResourcesName()(ResourcesName){
return ResourcesName{
        // api 3 : api 1
        "package_register": "rest/package",
        "package_entity": "rest/package",
        "group_register": "rest/group",
        "group_entity": "rest/group",
        "tag_register": "rest/tag",
        "tag_entity": "rest/tag",
        "rating_register": "rest/rating",
        "rating_entity": "rest/rating",
        "revision_register": "rest/revision",
        "revision_entity": "rest/revision",
        "license_list": "rest/licenses",
        "package_search": "search/package",
      };
}

// set Headers to make a request from client to CKAN
func (o *Gockan) SetRequestHeaders()(){
  //timeNow := time.Now().Format("2006/01/02 15:04:05")
  timeNow := time.Now().Format("Mon, 02 Mar 2006 15:04:05 GMT")
  o.reqHeaders = RequestHeaders{
    "Date": timeNow, // RFC 1123
    "Accept": "application/json;q=1.0, application/xml;q=0.5, */*;q=0.0",
    "Accept-Charset": "utf-8",
    "Accept-Encoding": "gzip",
    "User-Agent": o.GetUserAgent(),
  };
}

// get Headers to make a request from client to CKAN
func (o *Gockan) GetRequestHeaders()(RequestHeaders){
  return o.reqHeaders
}

// set ckan Client
func (o *Gockan) SetCkanClient(c *http.Client)(){
  if c == nil {
    c = http.DefaultClient
  }
  o.ckanClient = c
}

// get ckanClient
func (o *Gockan) GetCkanClient()(*http.Client){
  return o.ckanClient
}

// set user Agent
func (o *Gockan) SetUserAgent(u string)(){
  o.userAgent = u
}

// get user Agent
func (o *Gockan) GetUserAgent()(string){
  return o.userAgent
}

// set base URL
func (o *Gockan) SetBaseURL(urlBase string)(error){
  baseURL, err := url.Parse(urlBase)
  if err != nil {
    return err
  }
  o.baseURL = baseURL
  return nil
}

// get base URL
func (o *Gockan) GetBaseURL()(*url.URL){
  return o.baseURL
}

// set DataStores
func (o *Gockan) SetDataStore(datastore *GockanDataStore)(){
  o.dataStore = datastore
}

// get DataStores
func (o *Gockan) GetDataStore()(*GockanDataStore){
  return o.dataStore
}

// set Packages
func (o *Gockan) SetPackages(pckg *GockanPackages)(){
  o.packages = pckg
}

// get DataStores
func (o *Gockan) GetPackages()(*GockanPackages){
  return o.packages
}

// set apiversion
func (o *Gockan) SetApiVersion(api string)(){
  o.apiVersion = api
}

// get apiVersion
func (o *Gockan) GetApiVersion()(string){
  return o.apiVersion
}

// set apiKey
func (o *Gockan) SetApiKey(key string)(){
  o.apiKey = key
}

// get apiKey
func (o *Gockan) GetApiKey()(string){
  return o.apiKey
}
